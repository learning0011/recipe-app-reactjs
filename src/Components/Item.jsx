import PropTypes from "prop-types"; // Import prop-types
import styles from "./Item.module.css";

function Item({ item }) {
  return (
    <div className={styles.itemContainer}>
      <div className={styles.imageContainer}>
        <img
          className={styles.image}
          src={`https://spoonacular.com/cdn/ingredients_100x100/${item.image}`}
          alt=""
        ></img>
      </div>

      <div className={styles.nameContainer}>
        <div className={styles.name}>{item.name}</div>
        <div className={styles.amount}>
          {item.amount} {item.unit}
        </div>
      </div>
    </div>
  );
}

Item.propTypes = {
  item: PropTypes.object.isRequired,
};

export default Item;
