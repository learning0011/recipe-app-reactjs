import PropTypes from "prop-types"; // Import prop-types
import styles from "./foodItem.module.css";

function FoodItem({ food, setFoodID }) {
  return (
    <div className={styles.itemContainer}>
      <img className={styles.itemImage} src={food.image} alt=""></img>
      <div className={styles.itemContent}>
        <p className={styles.itemName}>{food.title}</p>
      </div>
      <div className={styles.buttonContainer}>
        <button
          onClick={() => {
            console.log(food.id);
            setFoodID(food.id);
          }}
          className={styles.itemButton}
        >
          View Recipe
        </button>
      </div>
    </div>
  );
}

FoodItem.propTypes = {
  food: PropTypes.object.isRequired,
  setFoodID: PropTypes.func.isRequired,
};

export default FoodItem;
