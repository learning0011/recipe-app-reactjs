import PropTypes from "prop-types"; // Import prop-types
import styles from "./container.module.css";

function Container({ children }) {
  return <div className={styles.parentContainer}>{children}</div>;
}

Container.propTypes = {
  // object can be used if single child?
  children: PropTypes.node.isRequired, // node is any?
};

export default Container;
