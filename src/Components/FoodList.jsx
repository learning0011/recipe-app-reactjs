import PropTypes from "prop-types"; // Import prop-types
import FoodItem from "./FoodItem";

function FoodList({ foodData, setFoodData, setFoodID }) {
  return (
    <div>
      {foodData.map((food) => (
        <FoodItem key={food.id} food={food} setFoodID={setFoodID}></FoodItem>
      ))}
    </div>
  );
}

FoodList.propTypes = {
  foodData: PropTypes.array.isRequired,
  setFoodData: PropTypes.func.isRequired,
  setFoodID: PropTypes.func.isRequired,
};

export default FoodList;
