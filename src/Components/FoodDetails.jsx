import PropTypes from "prop-types"; // Import prop-types
import { useEffect, useState } from "react";
import styles from "./foodDetails.module.css";
import ItemList from "./ItemList";

function FoodDetails({ foodID }) {
  const [food, setFood] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const endpoint = `https://api.spoonacular.com/recipes/${foodID}/information?`;
  const key = "4cbf79dcd783411fa3d792b61deb032f"; // "c4844e4e31de4a53ac681113acaea17c"; // "4cbf79dcd783411fa3d792b61deb032f"

  useEffect(() => {
    async function fetchFoodDetails() {
      let url = `${endpoint}apiKey=${key}`;
      console.log(url);
      let res = await fetch(url);
      let data = await res.json();
      console.log(data);

      if (res.ok === false) {
        let data = {
          image: "",
          title: "No Data",
          readyInMinutes: 0,
          servings: 0,
          vegetarian: false,
          vegan: false,
          pricePerServing: 0,
          analyzedInstructions: [
            {
              steps: [{ number: 0, step: "No API Data!" }],
            },
          ],
        };
        setFood(data);
        setIsLoading(false);
        return;
      }

      setFood(data);
      setIsLoading(false);
    }
    fetchFoodDetails();
  }, [endpoint, foodID]);
  return (
    <div>
      <div className={styles.recipeCard}>
        <h1 className={styles.recipeName}>{food.title}</h1>
        <img
          className={styles.recipeImage}
          src={food.image}
          alt={food.title}
        ></img>

        <div className={styles.recipeDetails}>
          <span>
            <strong>⏰{food.readyInMinutes} Minutes</strong>
          </span>
          <span>
            👨‍👩‍👧‍👦<strong>Serves {food.servings}</strong>
          </span>
          <span>
            <strong>
              {food.vegetarian ? "🥕 Vegetarian" : "🍖 Non-Vegetarian"}
            </strong>
          </span>
          <span>
            <strong>{food.vegan ? "Ⓥ Vegan" : "Non-Vegan"}</strong>
          </span>
        </div>
      </div>
      <div>
        💲
        <span>
          <strong>{Math.round(food.pricePerServing / 100)} Per serving</strong>
        </span>
      </div>

      <h2>Ingredients</h2>
      <ItemList food={food} isLoading={isLoading}></ItemList>

      <h2>Instructions</h2>

      <div className={styles.recipeInstructions}>
        <ol>
          {isLoading ? (
            <p>Loading...</p>
          ) : (
            food.analyzedInstructions[0].steps.map((step) => (
              <li key={step.number}> {step.step} </li>
            ))
          )}
        </ol>
      </div>
    </div>
  );
}

FoodDetails.propTypes = {
  foodID: PropTypes.string.isRequired,
};

export default FoodDetails;
