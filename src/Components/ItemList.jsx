import PropTypes from "prop-types"; // Import prop-types
import Item from "./Item";

function ItemList({ food, isLoading }) {
  return (
    <div>
      {!isLoading ? (
        food.extendedIngredients.map((item) => (
          <Item key={item.id} item={item}></Item>
        ))
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
}

ItemList.propTypes = {
  food: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
};

export default ItemList;
