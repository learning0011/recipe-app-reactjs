import PropTypes from "prop-types"; // Import prop-types
import styles from "./innerContainer.module.css";

function InnerContainer({ children }) {
  return <div className={styles.innerContainer}>{children}</div>;
}

InnerContainer.propTypes = {
  // object can be used if single child?
  children: PropTypes.node.isRequired, // node is any?
};

export default InnerContainer;
