import { useEffect, useState } from "react";
import PropTypes from "prop-types"; // Import prop-types
import styles from "./search.module.css";

const endpoint = "https://api.spoonacular.com/recipes/complexSearch?";
const key = "4cbf79dcd783411fa3d792b61deb032f"; // "c4844e4e31de4a53ac681113acaea17c"; // "4cbf79dcd783411fa3d792b61deb032f"
function Search({ foodData, setFoodData }) {
  const [query, setQuery] = useState("pizza");

  console.log(foodData);
  // Syntax of the useEffect hook
  useEffect(() => {
    async function fetchFood() {
      let url = `${endpoint}query=${query}&apiKey=${key}`;
      let res = await fetch(url);
      let data;
      if (res.status === 200) {
        console.log("Live Data is Used!");
        data = await res.json();
      } else {
        console.log("Default Data is Used!");
        data = {
          results: [
            {
              id: 782585,
              title: "Cannellini Bean and Asparagus Salad with Mushrooms",
              image: "https://spoonacular.com/recipeImages/782585-312x231.jpg",
              imageType: "jpg",
            },
            {
              id: 715497,
              title: "Berry Banana Breakfast Smoothie",
              image: "https://spoonacular.com/recipeImages/715497-312x231.jpg",
              imageType: "jpg",
            },
            {
              id: 715415,
              title: "Red Lentil Soup with Chicken and Turnips",
              image: "https://spoonacular.com/recipeImages/715415-312x231.jpg",
              imageType: "jpg",
            },
            {
              id: 716406,
              title: "Asparagus and Pea Soup: Real Convenience Food",
              image: "https://spoonacular.com/recipeImages/716406-312x231.jpg",
              imageType: "jpg",
            },
            {
              id: 644387,
              title: "Garlicky Kale",
              image: "https://spoonacular.com/recipeImages/644387-312x231.jpg",
              imageType: "jpg",
            },
          ],
          offset: 0,
          number: 5,
          totalResults: 5216,
        };
      }
      // if data.results.length === 0 { Show Something }
      setFoodData(data.results);

      // TO DELETE
      // let data = {
      //   results: [
      //     {
      //       id: 782585,
      //       title: "Cannellini Bean and Asparagus Salad with Mushrooms",
      //       image: "https://spoonacular.com/recipeImages/782585-312x231.jpg",
      //       imageType: "jpg",
      //     },
      //     {
      //       id: 715497,
      //       title: "Berry Banana Breakfast Smoothie",
      //       image: "https://spoonacular.com/recipeImages/715497-312x231.jpg",
      //       imageType: "jpg",
      //     },
      //     {
      //       id: 715415,
      //       title: "Red Lentil Soup with Chicken and Turnips",
      //       image: "https://spoonacular.com/recipeImages/715415-312x231.jpg",
      //       imageType: "jpg",
      //     },
      //     {
      //       id: 716406,
      //       title: "Asparagus and Pea Soup: Real Convenience Food",
      //       image: "https://spoonacular.com/recipeImages/716406-312x231.jpg",
      //       imageType: "jpg",
      //     },
      //     {
      //       id: 644387,
      //       title: "Garlicky Kale",
      //       image: "https://spoonacular.com/recipeImages/644387-312x231.jpg",
      //       imageType: "jpg",
      //     },
      //   ],
      //   offset: 0,
      //   number: 5,
      //   totalResults: 5216,
      // };
      // setFoodData(data.results);
    }
    fetchFood();
  }, [query, setFoodData]);

  return (
    <div className={styles.searchContainer}>
      <input
        className={styles.searchInput}
        type="text"
        value={query}
        onChange={(e) => setQuery(e.target.value)}
      ></input>
    </div>
  );
}

Search.propTypes = {
  foodData: PropTypes.array.isRequired,
  setFoodData: PropTypes.func.isRequired,
};

export default Search;
