import { useState } from "react";
import Nav from "./Components/Nav";
import Search from "./Components/Search";
import FoodList from "./Components/FoodList";
import "./App.css";
import Container from "./Components/Container";
import InnerContainer from "./Components/InnerContainer";
import FoodDetails from "./Components/FoodDetails";

function App() {
  const [foodData, setFoodData] = useState([]);
  const [foodID, setFoodID] = useState("656329");
  return (
    <div className="App">
      <Nav></Nav>
      <Search foodData={foodData} setFoodData={setFoodData}></Search>
      <Container>
        <InnerContainer>
          <FoodList
            foodData={foodData}
            setFoodData={setFoodData}
            setFoodID={setFoodID}
          ></FoodList>
        </InnerContainer>
        <InnerContainer>
          <FoodDetails foodID={foodID.toString()}></FoodDetails>
        </InnerContainer>
      </Container>
    </div>
  );
}

export default App;
